package net

import (
	"flag"
	"github.com/google/uuid"
	"gitlab.com/lopl/log"
	"net"
)

// ISession is
type ISession interface {
	GetID() uuid.UUID
	GetConn() net.Conn
	Send([]byte)
	Close()
}

// TCPServer is
type TCPServer struct {
	chanCloseListen chan struct{}
	listener        net.Listener
	mapClient       map[uuid.UUID]ISession
	ListenAddress   string
	isInit          bool
}

// Run is
func (p *TCPServer) Run() {
	if !p.isInit {
		log.Panic("TCPServer Init required")
	}

	p.chanCloseListen = make(chan struct{})
	p.mapClient = make(map[uuid.UUID]ISession)

	p.listen(p.ListenAddress)
}

// Init is
func (p *TCPServer) Init() {
	addr := flag.String("listen", "0.0.0.0:11111", "TCPServer listen address.")
	flag.Parse()
	p.ListenAddress = *addr

	p.isInit = true
}

// Listen is
func (p *TCPServer) listen(addr string) {
	var err error
	p.listener, err = net.Listen("tcp", addr)
	if err != nil {
		log.Panic(err)
		return
	}
	defer p.listener.Close()

	for {
		conn, err := p.listener.Accept()
		if err != nil {
			log.Panic(err)
			break
		}

		go func() {
			item := TCPServerSession{
				id:     uuid.New(),
				conn:   conn,
				server: p,
			}
			p.mapClient[item.id] = &item
			go item.read()
			// _, err := item.conn.Write([]byte(item.id.String()))
			// if err != nil {
			// 	sugar.Error(err)
			// }
		}()
	}
}

// Close is
func (p *TCPServer) Close() {
	if err := p.listener.Close(); err != nil {
		log.Error(err)
	}
}

// GetClients is
func (p *TCPServer) GetClients() map[uuid.UUID]ISession {
	return p.mapClient
}

// SendAll is all
func (p *TCPServer) SendAll(b []byte) {
	for _, v := range p.mapClient {
		v.Send(b)
	}
}

func (p *TCPServer) removeClient(id uuid.UUID) {
	item := p.mapClient[id]
	item.Close()

	delete(p.mapClient, id)
}

// TCPServerSession is
type TCPServerSession struct {
	id     uuid.UUID
	conn   net.Conn
	server *TCPServer
	quit   bool
}

// GetID is
func (p *TCPServerSession) GetID() uuid.UUID {
	return p.id
}

// GetConn is
func (p *TCPServerSession) GetConn() net.Conn {
	return p.conn
}

// Send is
func (p *TCPServerSession) Send(b []byte) {
	if p.quit {
		return
	}
	_, err := p.conn.Write(b)
	if err != nil {
		// sugar.Error(err)
	}
}

func (p *TCPServerSession) read() {
	if p.quit {
		return
	}
	defer func() {
		// p.quit = true
		id := p.GetID()
		p.server.removeClient(id)
		// sugar.Info("disconnect ", id)
	}()

	tmp := make([]byte, 256)
	for {
		_, err := p.conn.Read(tmp)
		if err != nil {
			break
			// if err == io.EOF {
			// 	break
			// }
		}
	}

	p.quit = true
}

// Close is
func (p *TCPServerSession) Close() {
	p.conn.Close()
}
