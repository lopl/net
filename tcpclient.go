package net

import (
	"net"
)

// client.go

// IClient is
type IClient interface {
	// Connect(string)
	OnConnect()
	OnDisconnect()
}

// SClient is
type SClient struct {
	conn net.Conn
	// OnConnect    func(*SClient)
	OnDisconnect func()
	// onConnect func()
	OnConnect func()
}

// New is
// func (p *SClient) New() *SClient {

// OnConnect is
// func (p *SClient) OnConnect(f func()) {
// 	p.onConnect = f
// }

// Connect is
func (p *SClient) Connect(s string) error {
	var err error
	if p.conn, err = net.Dial("tcp", s); err != nil {
		return err
	}
	p.OnConnect()
	return nil
}
